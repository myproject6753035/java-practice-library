import java.time.LocalDate;
import java.util.*;
import library.*;

public class Main {
    public static void main(String[] args) {
        Author author = new Author("Stephen", "King", LocalDate.now());

        Book book1 = new Book("Harry Potter", author, Genre.Fantasy, "1234");
        Book book2 = new Book("IT", author, Genre.Horror, "1235");
        Book book3 = new Book("How we met?", author, Genre.Romance, "1236");
        Book book4 = new Book("Elm Street Nightmares", author, Genre.Horror, "1237");
        Book book5 = new Book("World of Warcraft", author, Genre.Fantasy, "1238");

        Library library = new Library("Best Library");
        library.add(book1);
        library.add(book2);
        library.add(book3);
        library.add(book4);
        library.add(book5);

        List<Book> allBooks = library.getAllBooks();

        for (Book book : allBooks) {
            System.out.println(book.getDetails());
        }
    }
}
