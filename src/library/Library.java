package library;

import java.util.*;

public class Library {
    private final String name;
    private ArrayList<Book> books;

    public Library(String name) {
        this.name = name;
        this.books = new ArrayList<Book>();
    }

    public void add(Book book) {
        this.books.add(book);
    }

    public List<Book> getAllBooks() {
        return Collections.unmodifiableList(this.books);
    }
}
