package library;

import java.time.LocalDate;

public class Author {
    private final String firstName;
    private final String lastName;
    private final LocalDate birthDate;

    public Author(String firstName, String lastName, LocalDate birthDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
    }

    public String getFullName() {
        String fullName = this.firstName + " " + this.lastName;
        return fullName;
    }

    public LocalDate getBirthDate() {
        return this.birthDate;
    }
}
