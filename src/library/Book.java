package library;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Book {
    private final String title;
    private final Author author;
    private final Genre genre;
    private final String isbn;
    private LocalDate publicationDate;
    private BigDecimal priceUsd;
    private int pagesCount;

    public Book(String title, Author author, Genre genre, String isbn) {
        this.title = title;
        this.author = author;
        this.genre = genre;
        this.isbn = isbn;
    }

    public String getTitle() {
        return this.title;
    }

    public Author getAuthor() {
        return this.author;
    }

    public Genre getGenre() {
        return this.genre;
    }

    public String getIsbn() {
        return this.isbn;
    }

    public LocalDate getPublicationDate() {
        return this.publicationDate;
    }

    public void setPublicationDate(LocalDate publicationDate) {
        this.publicationDate = publicationDate;
    }

    public BigDecimal getPriceUsd() {
        return this.priceUsd;
    }

    public void setPriceUsd(BigDecimal priceUsd) {
        this.priceUsd = priceUsd;
    }

    public int getPagesCount() {
        return this.pagesCount;
    }

    public void setPagesCount(int pagesCount) {
        this.pagesCount = pagesCount;
    }

    public String getDetails() {
        return this.title + ", (" + author.getFullName() + ")";
    }
}
