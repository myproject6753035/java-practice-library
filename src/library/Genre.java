package library;

public enum Genre {
    Horror,
    Romance,
    Thriller,
    Fantasy
}
